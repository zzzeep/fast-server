#!/usr/bin/env bash

###################################################################################
################################## is root user ###################################
###################################################################################

if [ $(id -u) != '0' ]; then
    echo 'error: script needs root user.'
    exit 1
fi


###################################################################################
################################## versions #######################################
###################################################################################

nginx_v='1.11.2'
nginx_dir='1112'

php_v='7.0.8'
php_dir='708'

percona_v='5.7.13-6'
percona_dir='57136'

boost_v='1.59.0'
boost_dir='boost_1_59_0'

pcre_v='8.39'
pcre_dir='839'

zlib_v='1.2.8'
zlib_dir='128'

phpmyadmin_v='4.6.3'
phpmyadmin_dir='463'

yaconf_v='1.0.2'
yaconf_dir='102'

yac_v='2.0.1'
yac_dir='201'

###################################################################################
################################## some env work ##################################
###################################################################################

yum -y update

yum -y install gcc gcc-c++ automake autoconf libtool make cmake zlib zlib-devel pcre pcre-devel freetype freetype-devel libpng libpng-devel libevent libevent-devel libmcrypt libmcrypt-devel libjpeg libjpeg-devel jemalloc jemalloc-devel libxml2 libxml2-devel bzip2 bzip2-devel libcurl libcurl-devel glibc glibc-devel glib2 glib2-devel openssl openssl-devel bison bison-devel ncurses ncurses-devel gd gd-devel flex flex-devel libwebp libwebp-devel gettext gettext-devel gmp gmp-devel libaio libaio-devel boost boost-devel readline readline-devel

mkdir /usr/local/mysql
mkdir /usr/local/mysql/mydata
mkdir /usr/local/mysql/mytmp
mkdir /usr/local/mysql/mylog

mkdir /usr/local/nginx/html/yaconf
mkdir /usr/local/nginx/html/certbot
mkdir /usr/local/nginx/html/certbot-configs

###################################################################################
################################## make temp swap #################################
###################################################################################

dd if=/dev/zero of=/home/swap bs=1024 count=1000000
mkswap /home/swap
swapon /home/swap


###################################################################################
################################## groups & users #################################
###################################################################################

groupadd webmaster
groupadd mysql

useradd -g webmaster -M -d /usr/local/nginx webmaster -s /sbin/nologin
useradd -g mysql -M -d /usr/local/mysql mysql -s /sbin/nologin

chown -R mysql:mysql /usr/local/mysql

###################################################################################
################################## get packages ###################################
###################################################################################

cd /usr/local/src

wget http://mirrors.sohu.com/nginx/nginx-${nginx_v}.tar.gz
wget http://cn2.php.net/distributions/php-${php_v}.tar.gz
wget https://www.percona.com/downloads/Percona-Server-5.7/Percona-Server-${percona_v}/source/tarball/percona-server-${percona_v}.tar.gz
wget http://nchc.dl.sourceforge.net/project/boost/boost/${boost_v}/${boost_dir}.tar.gz
wget https://files.phpmyadmin.net/phpMyAdmin/${phpmyadmin_v}/phpMyAdmin-${phpmyadmin_v}-all-languages.zip
wget https://github.com/laruence/yaconf/archive/yaconf-${yaconf_v}.zip
wget https://github.com/laruence/yac/archive/yac-${yac_v}.zip

tar -zxvf nginx-${nginx_v}.tar.gz
tar -zxvf php-${php_v}.tar.gz
tar -zxvf percona-server-${percona_v}.tar.gz
tar -zxvf ${boost_dir}.tar.gz
unzip phpMyAdmin-${phpmyadmin_v}-all-languages.zip

tar -zxvf pcre-${pcre_v}.tar.gz
tar -zxvf zlib-${zlib_v}.tar.gz

# please check dirname before compile
unzip yaconf-${yaconf_v}.zip
unzip yac-${yac_v}.zip
mv yaconf-yaconf-${yaconf_v} yaconf${yaconf_dir}
mv yac-yac-${yac_v} yac${yac_dir}

mv nginx-${nginx_v}  nginx${nginx_dir}
mv php-${php_v}      php${php_dir}
mv percona-server-${percona_v}  percona${percona_dir}

mv pcre-${pcre_v}  pcre${pcre_dir}
mv zlib-${zlib_v}  zlib${zlib_dir}


###################################################################################
################################## install nginx ##################################
###################################################################################

cd /usr/local/src/nginx${nginx_dir}

./configure --prefix=/usr/local/nginx \
--user=webmaster \
--group=webmaster \
--with-pcre=/usr/local/src/pcre${pcre_dir} \
--with-zlib=/usr/local/src/zlib${zlib_dir} \
--with-pcre-jit \
--with-http_gzip_static_module \
--with-http_ssl_module \
--with-http_v2_module \
--with-ipv6 \
--with-file-aio \
--with-http_realip_module \
--with-http_sub_module \
--with-http_stub_status_module \
--with-http_random_index_module \
--with-http_flv_module \
--with-http_secure_link_module \
--with-http_mp4_module \
--with-threads \
--with-stream \
--with-stream_ssl_module \
--with-http_degradation_module
#--with-ld-opt="-ljemalloc"


make && make install

# nginx.conf
mv /usr/local/nginx/conf/nginx.conf /usr/local/nginx/conf/nginx.conf.bak
cp /usr/local/src/conf/nginx.conf /usr/local/nginx/conf/nginx.conf
mv /usr/local/src/conf/vhosts /usr/local/nginx/conf/vhosts

# certbot
cp /usr/local/src/conf/www.example.com.conf /usr/local/nginx/html/certbot/www.example.com.conf

# add phpmyadmin to nginx html dir
mv /usr/local/src/phpMyAdmin-${phpmyadmin_v}-all-languages /usr/local/nginx/html/phpmyadmin${phpmyadmin_dir}
cp /usr/local/nginx/html/phpmyadmin${phpmyadmin_dir}/config.sample.inc.php /usr/local/nginx/html/phpmyadmin${phpmyadmin_dir}/config.inc.php

# make sure nginx base dir's owner is webmaster
chown -R webmaster:webmaster /usr/local/nginx


#################################################################################
################################## install php ##################################
#################################################################################

cd /usr/local/src/php${php_dir}

./configure --prefix=/usr/local/php \
--enable-fpm \
--with-fpm-user=webmaster \
--with-fpm-group=webmaster \
--enable-libxml \
--with-libxml-dir \
--with-openssl \
--with-openssl-dir \
--with-zlib \
--with-zlib-dir \
--with-pcre-dir \
--enable-bcmath \
--with-bz2 \
--enable-calendar \
--enable-ctype \
--with-curl \
--enable-exif \
--enable-fileinfo \
--enable-filter \
--enable-ftp \
--with-jpeg-dir \
--with-png-dir \
--with-webp-dir \
--with-freetype-dir \
--with-gd \
--enable-gd-native-ttf \
--with-gettext \
--with-gmp \
--with-mhash \
--with-iconv \
--with-iconv-dir \
--enable-json \
--enable-mbstring \
--with-mcrypt \
--enable-zip \
--with-xmlrpc \
--with-mysqli \
--with-pdo-mysql \
--enable-session \
--enable-opcache \
--enable-simplexml \
--enable-soap \
--enable-sockets \
--enable-wddx

make && make install

# php conf
cp /usr/local/src/conf/php-fpm.conf  /usr/local/php/etc/php-fpm.conf
cp /usr/local/src/conf/www.conf      /usr/local/php/etc/php-fpm.d/www.conf
cp /usr/local/src/conf/php.ini       /usr/local/php/lib/php.ini

# yaconf
cd /usr/local/src/yaconf${yaconf_dir}
/usr/local/php/bin/phpize
./configure --with-php-config=/usr/local/php/bin/php-config
make && make install

# yac
cd /usr/local/src/yac${yac_dir}
/usr/local/php/bin/phpize
./configure --with-php-config=/usr/local/php/bin/php-config
make && make install

# make sure php base dir's owner is webmaster
chown -R webmaster:webmaster /usr/local/php


###################################################################################
################################## install mariadb ################################
###################################################################################

cd /usr/local/src/percona${percona_dir}

cmake -DCMAKE_INSTALL_PREFIX=/usr/local/mysql \
-DSYSCONFDIR=/etc \
-DMYSQL_DATADIR=/usr/local/mysql/mydata \
-DTMPDIR=/usr/local/mysql/mytmp \
-DSYSTEMD_PID_DIR=/usr/local/mysql/mylog \
-DMYSQL_UNIX_ADDR=/tmp/mysql.sock \
-DWITH_BOOST=/usr/local/src/${boost_dir} \
-DENABLED_LOCAL_INFILE=1 \
-DDEFAULT_CHARSET=utf8mb4 \
-DDEFAULT_COLLATION=utf8mb4_unicode_ci \
-DMYSQL_TCP_PORT=3306 \
-DWITH_XTRADB_STORAGE_ENGINE=1 \
-DWITH_INNOBASE_STORAGE_ENGINE=1 \
-DWITH_ARCHIVE_STORAGE_ENGINE=1 \
-DWITH_BLACKHOLE_STORAGE_ENGINE=1 \
-DWITH_PERFSCHEMA_STORAGE_ENGINE=1 \
-DWITH_EXTRA_CHARSETS=all \
-DWITH_LIBEVENT=bundled \
-DWITH_SSL=bundled \
-DWITH_ZLIB=system

make && make install

# mysql conf
mv /etc/my.cnf /etc/my.cnf.bakup
cp /usr/local/src/conf/my-percona.cnf /etc/my.cnf

# make sure mysql base dir's owner is mysql
chown -R mysql:mysql /usr/local/mysql

# initialize mariadb data
/usr/local/mysql/bin/mysqld --initialize-insecure --user=mysql \
--basedir=/usr/local/mysql \
--datadir=/usr/local/mysql/mydata

# add mysqld serivce
cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysqld
chmod +x /etc/init.d/mysqld

# make sure mysql base dir's owner is mysql again
chown -R mysql:mysql /usr/local/mysql


###################################################################################
################################## clean temp swap ################################
###################################################################################

swapoff /home/swap
rm -rf /home/swap

###################################################################################
################################## start all service ##############################
###################################################################################

/usr/local/nginx/sbin/nginx
/usr/local/php/sbin/php-fpm
service mysqld start

# make sure nginx base dir's owner is webmaster
chown -R webmaster:webmaster /usr/local/nginx
chown -R webmaster:webmaster /usr/local/php
chown -R mysql:mysql /usr/local/mysql

# change mariadb password
/usr/local/mysql/bin/mysqladmin -uroot password abcd

# certbot
cd /usr/local/src
git clone https://github.com/certbot/certbot.git

/usr/local/src/certbot/certbot-auto

cd
